# API SPEC

## Authentication
All API Must Use This authentication
- Header :
    - X-api-key: *your screet api key*

## Create Product
Request:
- Method : POST
- Endpoint : `/api/products`
- Header :
    - Content-Type: application/json
    - Accept: application/json
- Body :
```json
{
    "id" : "String, unique",
    "name" : "String",
    "price" : "Long",
    "quantity" : "Integer"
}
```

Response :

```json
{
    "code" : "Number",
    "status" : "String",
    "data" : {
        "id" : "String, unique",
        "name" : "String",
        "price" : "Long",
        "quantity" : "Integer",
        "createdAt" : "Date",
        "updatedAt" : "Date"
    }
}
```

## Get Product

Request:
- Method : GET
- Endpoint : `/api/products{id_product}`
- Header :
    - Accept: application/json

Response :

```json
{
    "code" : "Number",
    "status" : "String",
    "data" : {
        "id" : "String, unique",
        "name" : "String",
        "price" : "Long",
        "quantity" : "Integer",
        "createdAt" : "Date",
        "updatedAt" : "Date"
    }
}
```

## Update Product

Request:
- Method : PUT
- Endpoint : `/api/products{id_product}`
- Header :
    - Content-Type: application/json
    - Accept: application/json
- Body :
```json
{
    "name" : "String",
    "price" : "Long",
    "quantity" : "Integer"
}
```

Response :

```json
{
    "code" : "Number",
    "status" : "String",
    "data" : {
        "id" : "String, unique",
        "name" : "String",
        "price" : "Long",
        "quantity" : "Integer",
        "createdAt" : "Date",
        "updatedAt" : "Date"
    }
}
```

## List Product

Request:
- Method : GET
- Endpoint : `/api/products`
- Header :
    - Accept: application/json
- Query Param :
    - size: number,
    - page: number

Response :

```json
 {
    "code" : "Number",
    "status" : "String",
    "data" :
    [
        {
            "id" : "String, unique",
            "name" : "String",
            "price" : "Long",
            "quantity" : "Integer",
            "createdAt" : "Date",
            "updatedAt" : "Date"
        },
        {
            "id" : "String, unique",
            "name" : "String",
            "price" : "Long",
            "quantity" : "Integer",
            "createdAt" : "Date",
            "updatedAt" : "Date"
        }
    ]
}
```

## Delete Product

Request:
- Method : DELETE
- Endpoint : `/api/products{id_product}`
- Header :
    - Accept: application/json

Response :

```json
 {
    "code" : "Number",
    "status" : "String"
}
```