package rsb.kotlinrestful.repository

import org.springframework.data.jpa.repository.JpaRepository
import rsb.kotlinrestful.entity.Product

interface ProductRepository : JpaRepository<Product, String> {

}