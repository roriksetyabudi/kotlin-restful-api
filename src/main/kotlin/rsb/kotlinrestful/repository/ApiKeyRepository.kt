package rsb.kotlinrestful.repository

import org.springframework.data.jpa.repository.JpaRepository
import rsb.kotlinrestful.entity.ApiKey

interface ApiKeyRepository : JpaRepository<ApiKey, String> {
}