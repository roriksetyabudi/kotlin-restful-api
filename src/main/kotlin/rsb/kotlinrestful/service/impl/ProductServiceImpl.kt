package rsb.kotlinrestful.service.impl

import org.springframework.data.domain.PageRequest
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import rsb.kotlinrestful.entity.Product
import rsb.kotlinrestful.error.NotFoundException
import rsb.kotlinrestful.model.CreateProductRequest
import rsb.kotlinrestful.model.ListProductRequest
import rsb.kotlinrestful.model.ProductResponse
import rsb.kotlinrestful.model.UpdateProductRequest
import rsb.kotlinrestful.repository.ProductRepository
import rsb.kotlinrestful.service.ProductService
import rsb.kotlinrestful.validation.ValidationUtil
import java.util.*
import java.util.stream.Collectors

@Service
class ProductServiceImpl(
        val produceRepository: ProductRepository,
        val validationUtil: ValidationUtil

        ): ProductService {
    override fun create(createProductRequest: CreateProductRequest): ProductResponse {

        validationUtil.validate(createProductRequest)

        val product = Product(
                id = createProductRequest.id,
                name = createProductRequest.name,
                price = createProductRequest.price,
                quantity = createProductRequest.quantity,
                createdAt = Date(),
                updatedAt = null
        )
        produceRepository.save(product)

        return convertProductToProductResponse(product)
    }

    override fun get(id: String): ProductResponse {
        val product = findProductByIdOrThrowNotFound(id)
        return convertProductToProductResponse(product)

    }

    override fun update(id: String, updateProductRequest: UpdateProductRequest): ProductResponse {
        val product = findProductByIdOrThrowNotFound(id)

        validationUtil.validate(updateProductRequest)
        product.apply {
            name = updateProductRequest.name!!
            price = updateProductRequest.price!!
            quantity = updateProductRequest.quantity!!
            updatedAt = Date()
        }
        produceRepository.save(product)

        return convertProductToProductResponse(product)
    }

    override fun delete(id: String) {
        val product = findProductByIdOrThrowNotFound(id)
        produceRepository.delete(product)
    }

    override fun list(listProductRequest: ListProductRequest): List<ProductResponse> {
        val page = produceRepository.findAll(PageRequest.of(listProductRequest.page, listProductRequest.size))
        val product: List<Product> =  page.get().collect(Collectors.toList())

        return product.map { convertProductToProductResponse(it) }
    }
    private fun findProductByIdOrThrowNotFound(id: String): Product {
        val product =  produceRepository.findByIdOrNull(id)
        if(product == null) {
            throw NotFoundException()
        } else {
            return product
        }
    }
    private fun convertProductToProductResponse(product: Product): ProductResponse {
        return ProductResponse(
            id = product.id,
            name = product.name,
            price = product.price,
            quantity = product.quantity,
            createdAt = product.createdAt,
            updatedAt = product.updatedAt
        )
    }
}