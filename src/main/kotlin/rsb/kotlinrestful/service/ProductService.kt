package rsb.kotlinrestful.service

import rsb.kotlinrestful.model.CreateProductRequest
import rsb.kotlinrestful.model.ListProductRequest
import rsb.kotlinrestful.model.ProductResponse
import rsb.kotlinrestful.model.UpdateProductRequest

interface ProductService {

    fun create(createProductRequest: CreateProductRequest): ProductResponse

    fun get(id: String): ProductResponse

    fun update(id: String, updateProductRequest: UpdateProductRequest) : ProductResponse

    fun delete(id: String)

    fun list(listProductRequest: ListProductRequest) : List<ProductResponse>

}