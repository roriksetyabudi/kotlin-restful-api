package rsb.kotlinrestful.config

import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import rsb.kotlinrestful.entity.ApiKey
import rsb.kotlinrestful.repository.ApiKeyRepository

@Component
class ApiKeyConfiguration(val apiKeyRepository: ApiKeyRepository): ApplicationRunner {

    val apiKey = "SCREET"

    override fun run(args: ApplicationArguments?) {
        if(!apiKeyRepository.existsById(apiKey)) {
            val entity = ApiKey(apiKey)
            apiKeyRepository.save(entity)


        }
    }
}