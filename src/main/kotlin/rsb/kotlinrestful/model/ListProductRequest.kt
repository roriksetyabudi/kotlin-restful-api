package rsb.kotlinrestful.model

data class ListProductRequest(

    val page: Int,

    val size: Int

)